### **Description**
concept of android app for fictive company DJ in the Box.
More about whole project could be found in folder *docs*.

---
### **Technology**
Android

---
### **Year**
2015

---
### **Screenshot**
Black theme:  
![](/README/screen0_postcard.jpg)

White theme:  
![](/README/screen_white_postcard.jpg)

tablet:  
![](/README/screen4.jpg)