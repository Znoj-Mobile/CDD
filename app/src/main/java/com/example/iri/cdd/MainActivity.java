package com.example.iri.cdd;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import it.neokree.materialnavigationdrawer.MaterialNavigationDrawer;


public class MainActivity extends MaterialNavigationDrawer {

    private F_objednavky f_objednavky = new F_objednavky();
    private F_playlist f_playlist = new F_playlist();
    private F_sdileni_informaci f_sdileni_informaci = new F_sdileni_informaci();
    private F_statistika f_statistika = new F_statistika();
    private F_kredit f_kredit = new F_kredit();



    /**
     * Zde se nastavuje levé vysouvací menu
     */
    @Override
    public void init(Bundle savedInstanceState) {
        //logo
        setDrawerHeaderImage(R.drawable.menu_logo_dj_box);

        addDivisor();
        addSection(newSection(getResources().getString(R.string.prihlasit), f_objednavky));


        addSubheader(getResources().getString(R.string.objednavky));
        addSection(newSection(getResources().getString(R.string.pisnicky), f_objednavky));
        addSection(newSection(getResources().getString(R.string.jidlo), f_objednavky));
        addSection(newSection(getResources().getString(R.string.piti), f_objednavky));

        addDivisor();
        addSection(newSection(getResources().getString(R.string.playlist), f_playlist));

        addDivisor();
        addSection(newSection(getResources().getString(R.string.sdileni_informaci), f_sdileni_informaci));

        addSubheader(getResources().getString(R.string.statistika));
        addSection(newSection(getResources().getString(R.string.globalni), f_statistika));
        addSection(newSection(getResources().getString(R.string.uctu), f_statistika));
        addSection(newSection(getResources().getString(R.string.konkretni_dib), f_statistika));

        addSubheader(getResources().getString(R.string.kredit));
        addSection(newSection(getResources().getString(R.string.kontrola_stavu), f_kredit));
        addSection(newSection(getResources().getString(R.string.dobijeni), f_kredit));
        addSection(newSection(getResources().getString(R.string.platby_objednavek), f_kredit));
        enableToolbarElevation();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
